package datastructure;

import java.util.ArrayList;
import java.util.List;

public class Grid<T> implements IGrid<T> {
    private final List<T> grid;
	private final int columns;
	private final int rows;


    public Grid(int rows, int columns, T initial) {
        if (rows <= 0 || columns <= 0) {
			throw new IllegalArgumentException();
		}

		this.columns = columns;
		this.rows = rows;
        grid = new ArrayList<T>(columns * rows);
        for (int i = 0; i < columns * rows; ++i) {
            grid.add(initial);
        }
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public int numRows() {
        return rows;
    }

    public void checkLocation(Location loc) {
		if (!isOnGrid(loc)) {
			throw new IndexOutOfBoundsException();
		}
	}

    @Override
    public void set(Location loc, T element) {
        grid.set(coordinateToIndex(loc), element);
    }

    private int coordinateToIndex(Location loc) {
		return loc.row + loc.col * rows;
	}

    @Override
    public T get(Location loc) {
        checkLocation(loc);
        return grid.get(coordinateToIndex(loc));
    }

    @Override
    public Iterable<Location> locations() {
        return new GridLocationIterator(numRows(), numColumns());
    }

    @Override
    public boolean isOnGrid(Location loc) {
        if (loc.row < 0 || loc.row >= rows) {
			return false;
		}

		return loc.col >= 0 && loc.col < columns;
    }

    @Override
    public IGrid<T> copy() {
        IGrid<T> newGrid = new Grid<T>(numRows(), numColumns(), null);

		for (Location loc : this.locations()) {
			newGrid.set(loc, this.get(loc));
		}
		return newGrid;
    }
    
}
